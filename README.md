# Monitoring server dengan flask dan socketing

**Flask**

FLask merupakan WSGI web application framework yang ringan. Dibuat untuk
memulai dengan mudah dan cepat, dengan kemampuan untuk dikembangkan aplikasi kompleks.
Flask menggabungkan Werkzeug dan Jinja menjadi satu.

**Socketing**

Socketing merupakan cara menghubungkan dua node dalam jaringan untuk berkomunikasi satu sama lain.
Satu node/socket listen pada port dalam IP tertentu, sedangkan node/socket lainnya menggapai ke 
node lainnya untuk membentuk suatu koneksi. Server sebagai listener socket sedangkan
client menggapai server.

**How to run**



**Author**

Tugas besar mata kuliah Sister dari kelompok 10 IF 40-01
oleh Gary Andersen dan Kinegar Hadinawa

**Reference**


1.  https://palletsprojects.com/p/flask/

2.  Slide kuliah Pemograman Jaringan "02.Socket Level Programing"
